#ifndef LIVEGENERATOR_H
#define LIVEGENERATOR_H

#include "commandlineparser.h"
#include "mountunit.h"
#include "path.h"
#include <string>

class LiveGenerator {
public:
    LiveGenerator(const CommandlineParser& parser);

    bool writeUnits(const Path& path);

    Path basedir() const;
    void setBasedir(const Path& basedir);

private:
    bool writeBackingStoreUnit(const Path& path);
    bool writeReadonlyFSUnit(const Path& path, const MountUnit& backingStore);
    bool writeReadWriteFSUnit(const Path& path, const MountUnit& rofs);
    bool writeSysrootFSUnit(const Path& path, const MountUnit& rwfs, const MountUnit& rofs);
    bool makeRequiresLink(const Path& path, const std::string& unit, const std::string& requires);

    CommandlineParser m_parser;
    Path m_basedir;
};

#endif // LIVEGENERATOR_H
