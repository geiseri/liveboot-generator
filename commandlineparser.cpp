#include "commandlineparser.h"
#include "config.h"
#include "path.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <stdlib.h>

CommandlineParser::CommandlineParser()
    : m_isLive(false)
{
    if (getenv("CMDLINE") == NULL) {
        parseProcCommandline("/proc/cmdline");
    } else {
        parseProcCommandline(getenv("CMDLINE"));
    }
}

bool CommandlineParser::isLive() const
{
    return m_isLive;
}

Path CommandlineParser::backingstore() const
{
    return m_backingstore;
}

Path CommandlineParser::rwfs() const
{
    return m_rwfs;
}

Path CommandlineParser::rofs() const
{
    return m_rofs;
}

void CommandlineParser::dumpStructure() const
{
    std::cerr << "Structure: " << std::endl;
    std::cerr << "m_rwfs: " << m_rwfs.path() << std::endl;
    std::cerr << "m_rofs: " << m_rofs.path() << std::endl;
    std::cerr << "m_backingstore: " << m_backingstore.path() << std::endl;
    std::cerr << "m_isLive: " << m_isLive << std::endl;
}

bool isFlag(const std::string& argument)
{
    return (argument.find('=') == std::string::npos);
}

std::string getArgumentValue(std::string argument)
{
    std::size_t start = argument.find('=');
    if (start == std::string::npos) {
        return argument;
    } else {
        return argument.substr(start + 1);
    }
}

std::string getString(const std::string& key, const std::string& line)
{
    // find string, find next whitespace, retun substring

    std::size_t pos1 = line.find(key, 0);
    if (pos1 != std::string::npos) {
        std::size_t pos2 = line.find_first_of(" \t\n", pos1) - pos1;
        return line.substr(pos1, pos2);
    }

    return std::string();
}

Path convertUUIDToPath(const std::string& input)
{
    // split UUID=<guid> into /dev/disk/by-uuid/<lowercase guid>
    std::string uuid = getArgumentValue(input);
    std::transform(uuid.begin(), uuid.end(), uuid.begin(), ::tolower);
    return Path("/dev/disk/by-uuid/" + uuid);
}

Path convertLabelToPath(const std::string& input)
{
    // split LABEL=<label> into /dev/disk/by-label/<label>
    std::string label = getArgumentValue(input);
    return Path("/dev/disk/by-label/" + label);
}

bool isUUIDMount(const std::string& mount)
{
    return mount.find("UUID") != std::string::npos;
}

bool isLabelMount(const std::string& mount)
{
    return mount.find("LABEL") != std::string::npos;
}

bool isDevMount(const std::string& mount)
{
    return mount.find("/dev") != std::string::npos;
}

bool isPathMount(const std::string& mount)
{
    return mount.find("/") == 0;
}

Path getMountPath(const std::string& mount)
{
    Path devpath;

    if (isUUIDMount(mount)) {
        devpath = convertUUIDToPath(mount);
    } else if (isLabelMount(mount)) {
        devpath = convertLabelToPath(mount);
    } else if (isDevMount(mount)) {
        devpath = Path(getArgumentValue(mount));
    } else if (isPathMount(mount)) {
        devpath = (Path(LIVE_DIR) + "image").fileAt(getArgumentValue(mount));
    } else {
        std::cerr << "Mount type " << mount << " is not supported!" << std::endl;
    }

    return devpath;
}

void CommandlineParser::parseProcCommandline(const std::string& file)
{
    std::ifstream cmdline(file, std::ifstream::in);
    std::string contents;
    std::getline(cmdline, contents);

    std::cout << "Commandline: " << contents << std::endl;

    if (contents.find("live") != std::string::npos) {
        m_isLive = true;
        m_backingstore = getMountPath(getArgumentValue(getString("live", contents)));
        m_rofs = getMountPath(getArgumentValue(getString("roimg", contents)));
        std::string rwpath = getArgumentValue(getString("rwimg", contents));
        if (rwpath == "tmpfs" || rwpath == std::string()) {
            m_rwfs = Path();
        } else {
            std::cout << "rwfs " << rwpath << std::endl;
            m_rwfs = getMountPath(rwpath);
        }
    }
}

bool CommandlineParser::isValid() const
{
    if (m_isLive) {
        if (!m_rofs.isValid())
            return false;
        if (!m_rwfs.isValid())
            return false;
        if (!m_backingstore.isValid())
            return false;
    }

    return true;
}
