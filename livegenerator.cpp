#include "livegenerator.h"
#include "config.h"
#include "mountunit.h"
#include "path.h"
#include <iostream>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

LiveGenerator::LiveGenerator(const CommandlineParser& parser)
    : m_parser(parser)
    , m_basedir(LIVE_DIR)
{
}

bool LiveGenerator::writeUnits(const Path& path)
{
    if (path.makePath()) {
        return writeBackingStoreUnit(path);
    } else {
        std::cerr << "writeUnits failed to make path: " << path << std::endl;
        return false;
    }
}

bool LiveGenerator::makeRequiresLink(const Path& path, const std::string& unit, const std::string& requires)
{
    Path directory = path + unit;
    Path src = path.fileAt(requires);
    Path dest = directory.fileAt(requires);

    if (directory.makePath()) {
        return (0 == ::symlink(src.path().c_str(), dest.path().c_str()));
    } else {
        std::cerr << "makeRequiresLink failed to make path: " << directory << std::endl;
        return false;
    }
}

bool LiveGenerator::writeBackingStoreUnit(const Path& path)
{
    MountUnit backingStore(path);

    std::list<std::string> beforeUnits;

    beforeUnits.push_back("initrd-root-fs.target");

    backingStore.setBeforeUnits(beforeUnits);
    backingStore.setMountSource(m_parser.backingstore());
    backingStore.setMountDestination(m_basedir.fileAt("image"));
    backingStore.setOptions("ro");
    backingStore.setFilesystemType("auto");
    if (backingStore.createMountpoint()) {

        return backingStore.writeMountFile() && writeReadonlyFSUnit(path, backingStore);
    } else {
        std::cerr << "writeBackingStoreUnit failed to make path: " << backingStore << std::endl;
        return false;
    }
}

bool LiveGenerator::writeReadonlyFSUnit(const Path& path, const MountUnit& backingStore)
{
    MountUnit rofs(path);

    std::list<std::string> beforeUnits;
    std::list<std::string> requiredUnits;
    std::list<std::string> afterUnits;

    beforeUnits.push_back("initrd-root-fs.target");
    requiredUnits.push_back(backingStore.unitFileName());
    afterUnits.push_back(backingStore.unitFileName());

    rofs.setRequiredUnits(requiredUnits);
    rofs.setAfterUnits(afterUnits);
    rofs.setBeforeUnits(beforeUnits);
    rofs.setMountSource(m_parser.rofs());
    rofs.setMountDestination(m_basedir.fileAt("rofs"));

    if (m_parser.rofs().path().find("/dev/disk") == std::string::npos) {
        rofs.setOptions("loop,ro");
        rofs.setDefaultDependencies(false);
    } else {
        rofs.setOptions("ro");
    }
    if (rofs.createMountpoint()) {
        return rofs.writeMountFile() && writeReadWriteFSUnit(path, rofs);
    } else {
        std::cerr << "writeReadonlyFSUnit failed to make path: " << rofs << std::endl;
        return false;
    }
}

bool LiveGenerator::writeReadWriteFSUnit(const Path& path, const MountUnit& rofs)
{
    MountUnit rwfs(path);

    std::list<std::string> beforeUnits;

    beforeUnits.push_back("initrd-root-fs.target");

    rwfs.setBeforeUnits(beforeUnits);
    rwfs.setMountSource(m_parser.rwfs());
    rwfs.setMountDestination(m_basedir.fileAt("rwfs"));
    rwfs.setOptions("rw");

    if (m_parser.rwfs() == Path()) {
        rwfs.setFilesystemType("tmpfs");
    } else {
        rwfs.setFilesystemType("auto");
    }

    if (rwfs.createMountpoint()) {
        return rwfs.writeMountFile() && writeSysrootFSUnit(path, rwfs, rofs);
    } else {
        std::cerr << "writeReadWriteFSUnit failed to createMountpoint: " << rwfs << std::endl;

        return false;
    }
}

bool LiveGenerator::writeSysrootFSUnit(const Path& path, const MountUnit& rwfs, const MountUnit& rofs)
{
    MountUnit sysroot(path);

    std::list<std::string> beforeUnits;
    std::list<std::string> requiredUnits;
    std::list<std::string> afterUnits;

    beforeUnits.push_back("initrd-root-fs.target");
    requiredUnits.push_back(rwfs.unitFileName());
    afterUnits.push_back(rwfs.unitFileName());
    requiredUnits.push_back(rofs.unitFileName());
    afterUnits.push_back(rofs.unitFileName());

    sysroot.setRequiredUnits(requiredUnits);
    sysroot.setAfterUnits(afterUnits);
    sysroot.setBeforeUnits(beforeUnits);
    sysroot.setMountSource(Path());
    sysroot.setMountDestination(Path("/sysroot"));

    sysroot.setOptions("br=" + rwfs.mountDestination().path() + ":" + rofs.mountDestination().path() + "=ro");
    sysroot.setFilesystemType("aufs");

    if (makeRequiresLink(path, "initrd-root-fs.target.requires", sysroot.unitFileName())) {
        if (sysroot.createMountpoint()) {
            return sysroot.writeMountFile();
        } else {
            std::cerr << "writeSysrootFSUnit failed to createMountpoint: " << sysroot << std::endl;
            return false;
        }
    } else {
        std::cerr << "writeSysrootFSUnit failed to makeRequiresLink: " << path << std::endl;
        return false;
    }
}

Path LiveGenerator::basedir() const
{
    return m_basedir;
}

void LiveGenerator::setBasedir(const Path& basedir)
{
    m_basedir = basedir;
}
