#ifndef PATH_H
#define PATH_H

#include <string>
#include <vector>

class Path {
public:
    Path();
    Path(const std::string& root);

    std::string path() const;
    std::string systemdPath() const;
    bool makePath() const;
    Path fileAt(const std::string& fileName) const;
    bool isValid() const;

private:
    friend Path operator+(const Path& left, const std::string& right);
    friend Path operator+(const Path& left, const Path& right);
    friend bool operator==(const Path& left, const Path& right);
    friend std::ostream& operator<<(std::ostream& os, const Path& path);

    std::vector<std::string> m_path;
};

Path operator+(const Path& left, const std::string& right);
Path operator+(const Path& left, const Path& right);
bool operator==(const Path& left, const Path& right);
std::ostream& operator<<(std::ostream& os, const Path& path);

#endif // PATH_H
