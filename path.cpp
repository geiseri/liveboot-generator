#include "path.h"

#include <iostream>
#include <limits.h>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

std::vector<std::string> splitString(const std::string& s, char delim)
{
    std::vector<std::string> elems;
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        if (item.length() > 0)
            elems.push_back(item);
    }
    return elems;
}

std::string joinStringList(const std::vector<std::string>& sl, char delim, bool absolute)
{
    std::stringstream ss;

    for (size_t idx = 0; idx < sl.size(); ++idx) {
        if (idx != 0 || absolute)
            ss << delim;
        ss << sl[idx];
    }

    return ss.str();
}

Path::Path()
{
}

Path::Path(const std::string& root)
{
    m_path = splitString(root, '/');
}

std::string Path::path() const
{
    return joinStringList(m_path, '/', true);
}

std::string Path::systemdPath() const
{
    return joinStringList(m_path, '-', false);
}

bool Path::makePath() const
{
    struct stat sb;
    std::stringstream ss;
    for (std::string path : m_path) {
        ss << '/' << path;
        if (::stat(ss.str().c_str(), &sb)) {
            if (errno != ENOENT || (::mkdir(ss.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) && errno != EEXIST && errno != EACCES)) {
                std::cerr << "mkdir error:" << errno << std::endl;
                return false;
            }
        } else if (!S_ISDIR(sb.st_mode)) {
            return false;
        }
    }
    return true;
}

Path Path::fileAt(const std::string& fileName) const
{
    return (*this + fileName);
}

bool Path::isValid() const
{
    return !m_path.empty();
}

Path operator+(const Path& left, const std::string& right)
{
    return left + Path(right);
}

Path operator+(const Path& left, const Path& right)
{
    Path result(left);
    result.m_path.insert(result.m_path.end(), right.m_path.begin(), right.m_path.end());
    return result;
}

bool operator==(const Path& left, const Path& right)
{
    return left.m_path == right.m_path;
}

std::ostream& operator<<(std::ostream& os, const Path& path)
{
    os << path.path();
    return os;
}
