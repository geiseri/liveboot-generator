#ifndef MOUNTUNIT_H
#define MOUNTUNIT_H

#include <list>
#include <string>

#include "path.h"

class MountUnit {
public:
    MountUnit(const Path& unitDirectory);

    bool writeMountFile();
    bool createMountpoint();

    std::string unitFileName() const;

    Path mountSource() const;
    void setMountSource(const Path& mountSource);

    Path mountDestination() const;
    void setMountDestination(const Path& mountDestination);

    std::string filesystemType() const;
    void setFilesystemType(const std::string& filesystemType);

    std::list<std::string> requiredUnits() const;
    void setRequiredUnits(const std::list<std::string>& requiredUnits);

    std::list<std::string> afterUnits() const;
    void setAfterUnits(const std::list<std::string>& afterUnits);

    std::list<std::string> beforeUnits() const;
    void setBeforeUnits(const std::list<std::string>& beforeUnits);

    std::string options() const;
    void setOptions(const std::string& options);

    bool defaultDependencies() const;
    void setDefaultDependencies(bool enable);

    std::list<std::string> conflictUnits() const;
    void setConflictUnits(const std::list<std::string>& conflictUnits);

private:
    friend std::ostream& operator<<(std::ostream& os, const MountUnit& unit);
    Path m_unitDirectory;
    bool m_defaultDependencies;
    Path m_mountSource;
    Path m_mountDestination;
    std::string m_filesystemType;
    std::string m_options;
    std::list<std::string> m_requiredUnits;
    std::list<std::string> m_afterUnits;
    std::list<std::string> m_beforeUnits;
    std::list<std::string> m_conflictUnits;
};

std::ostream& operator<<(std::ostream& os, const MountUnit& unit);

#endif // MOUNTUNIT_H
