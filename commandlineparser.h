#ifndef COMMANDLINEPARSER_H
#define COMMANDLINEPARSER_H
#include "path.h"
#include <list>
#include <string>

class CommandlineParser {
public:
    CommandlineParser();

    bool isLive() const;
    Path backingstore() const;
    Path rwfs() const;
    Path rofs() const;

    void dumpStructure() const;

    bool isValid() const;

private:
    void parseProcCommandline(const std::string& file);

    // live boot
    bool m_isLive;
    Path m_backingstore;
    Path m_rwfs;
    Path m_rofs;
};

#endif // COMMANDLINEPARSER_H
