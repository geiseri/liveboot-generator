#include "commandlineparser.h"
#include "livegenerator.h"
#include <iostream>

int main(int argc, char* argv[])
{

    if (argc > 1 && argc != 4) {
        return 1;
    }

    Path dest(argv[1]);

    CommandlineParser parser;
    parser.dumpStructure();

    if (parser.isLive()) {
        LiveGenerator generator(parser);
        return (generator.writeUnits(dest) == true);
    }

    return 0;
}
